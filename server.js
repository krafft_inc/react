var express = require('express');
var graphqlHttp = require('express-graphql');
var schema = require('./schema/schema');
var async = require('async');
var path = require('path');
var config = require('./config.js');
var errTo = require('errto');
var migration = require('./schema/migrate');
var database = require('./schema/database');
var db = require('./schema/connection');
var r = require('rethinkdb');
var app = express();

app.use('/graphql', graphqlHttp({schema: schema}));

app.use('/relay', express.static('./node_modules/react-relay/dist'));

app.use('/', express.static('./public'));

migration.migrate(function(){
    database.initialize((function(){
        app.listen(config.express.port);
        console.log('Listening on port ' + config.express.port);
    }))
});