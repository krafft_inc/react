var r = require('rethinkdb');
var config = require('../config.js');
var errTo = require('errto');

function getConnection(callback){
    r.connect(config.rethinkdb, errTo(callback, function(connection){
        callback(null, connection)
    }));
};

module.exports = {
    getConnection:getConnection,
}