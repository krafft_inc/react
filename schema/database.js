var r = require('rethinkdb');
var async = require('async');
var path = require('path');
var config = require(path.resolve(__dirname, '../config.js'));
var errTo = require ('errto');

var companiesModel = require('../models').companies;
var vacanciesModel = require('../models').vacancies;

var companies =[];
var vacancies =[];

function initialize(callback){
  async.waterfall([
    function fetchCompanies(callback) {
      companiesModel.getCompanies(function(err, result){
        companies = result;
        callback(err, result);
      });
    },
    function fetchVacancies(result, callback) {
      vacanciesModel.getVacancies(function(err, result){
        vacancies = result;
        callback(err, result);
      });
    }
    ], function(err, result) {
    if(err) {
      console.error(err);
      process.exit(1);
      return;
    }
    callback()
  });
}

module.exports = {
  getCompany: function(id) { return companies.filter(function(c) { return c.id == id })[0] },
  getVacancy: function(id) { return vacancies.filter(function(v) { return v.id == id })[0] },
  getVacanciesByCompany: function(companyId) { return vacancies.filter(function(v) { return v.companyId == companyId }) },
  initialize:initialize
}

