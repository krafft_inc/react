var GraphQL = require('graphql');
var GraphQLRelay = require('graphql-relay');
var db = require('./database');
var companies = require('../models').companies;
var vacancies = require('../models').vacancies;


var nodeDefinitions = GraphQLRelay.nodeDefinitions(function(globalId) {
  var idInfo = GraphQLRelay.fromGlobalId(globalId);

  if (idInfo.type == 'Company') {
    return db.getCompany(idInfo.id)
  } else if (idInfo.type == 'Vacancy') {
    return db.getVacancy(idInfo.id)
  }

  return null;

});

// We can now use the Node interface in the GraphQL types of our schema

var vacancyType = new GraphQL.GraphQLObjectType({
  name: 'Vacancy',
  description: 'Great vacancy',


  isTypeOf: function(obj) { return obj instanceof companies.baseClass || true},
  fields: {
    id: GraphQLRelay.globalIdField('Vacancy'),
    position: {
      type: GraphQL.GraphQLString,
      description: 'Position',
    },
    description:{
      type: GraphQL.GraphQLString,
      description: 'Description',
    },
    salary:{
      type: GraphQL.GraphQLString,
      description: 'Salary',
    },
    companyId:{
      type: GraphQL.GraphQLString,
      description: 'Company id',
    }
  },

  interfaces: [nodeDefinitions.nodeInterface],
});

var companyType = new GraphQL.GraphQLObjectType({
  name: 'Company',
  description: 'A great Company',
  isTypeOf: function(obj) { return obj instanceof vacancies.baseClass || true},

  fields: function() {
    return {
      id: GraphQLRelay.globalIdField('Company'),
      name: {
        type: GraphQL.GraphQLString,
        description: 'The name of the company',
      },
      address:{
        type: GraphQL.GraphQLString,
        description: 'Address of the company',
      },
      membersCount:{
        type: GraphQL.GraphQLString,
        description: 'Company members quantity',
      },

      vacancies: {
        description: 'A company\'s collection of vacancies',


        type: GraphQLRelay.connectionDefinitions({name: 'Vacancy', nodeType: vacancyType}).connectionType,
        args: GraphQLRelay.connectionArgs,

        resolve: function(company, args) {

          return GraphQLRelay.connectionFromArray(db.getVacanciesByCompany(company.id), args)
        },
      },
    }
  },
  interfaces: [nodeDefinitions.nodeInterface],
})

// Now we can bundle our types up and export a schema
// GraphQL expects a set of top-level queries and optional mutations (we have
// none in this simple example so we leave the mutation field out)
module.exports = new GraphQL.GraphQLSchema({
  query: new GraphQL.GraphQLObjectType({
    name: 'Query',
    fields: {
      // Relay needs this to query Nodes using global IDs
      node: nodeDefinitions.nodeField,
      // Our own root query field(s) go here
      company1: {
        type: companyType,
        resolve: function() {
          console.log(db.getCompany(1));
          return db.getCompany(1)
        },
      },
      company2: {
        type: companyType,
        resolve: function() {
          return db.getCompany(2)
        },
      },
      company3: {
        type: companyType,
        resolve: function() {
          return db.getCompany(3)
        },
      },
    },
  }),
})
