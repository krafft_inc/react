var r = require('rethinkdb');
var async = require('async');
var path = require('path');
var config = require(path.resolve(__dirname, '../config.js'));
var vacancies = require(path.resolve(__dirname, '../vacancies.json'));
var companies = require(path.resolve(__dirname, '../companies.json'));



function migrate(callback){
    async.waterfall([
        function connect(callback) {
            r.connect(config.rethinkdb, callback);
        },
        function createDatabase(connection, callback) {
            //Create the database if needed.
            r.dbList().contains(config.rethinkdb.db).do(function(containsDb) {

                return r.branch(containsDb, {created: 0}, r.dbCreate(config.rethinkdb.db));

            }).run(connection, function(err) {
                callback(err, connection);
            });
        },
        function createCompaniesTable(connection, callback) {
            //Create the table if needed.
            r.tableList().contains('companies').do(function(containsTable) {

                return r.branch(containsTable, {created: 0}, r.tableCreate('companies'));

            }).run(connection, function(err) {
                callback(err, connection);
            });
        },
        function createVacanciesTable(connection, callback) {
            //Create the table if needed.
            r.tableList().contains('vacancies').do(function(containsTable) {

                return r.branch(containsTable, {created: 0}, r.tableCreate('vacancies'));

            }).run(connection, function(err) {
                callback(err, connection);
            });
        },
        function createVacancies(connection, callback) {
            r.table('vacancies').insert(vacancies, {returnChanges: true}).run(connection, function(err, result) {
                callback(err, connection);
            });
        },
        function createCompanies(connection, callback) {
            r.table('companies').insert(companies, {returnChanges: true}).run(connection, function(err, result) {
                callback(err, connection);
            });
        }
    ], function(err, connection) {
        if(err) {
            console.error(err);
            process.exit(1);
            return;
        }
        connection.close();
        callback()
    });

}

module.exports = {
    migrate:migrate
};