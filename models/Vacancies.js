var db = require('../schema/connection');
var errTo = require ('errto');
var r = require('rethinkdb');
var _ = require('underscore');

function Vacancy(options){
    this.position = options.position;
    this.description = options.description;
    this.salary = options.salary;
    this.companyId = options.companyId;
    this.id = options.id
}

function getVacancies(callback){

    db.getConnection(errTo(callback, function(connecion){
        r.table('vacancies').run(connecion, errTo(callback, function(cursor){

            cursor.toArray(function(err, result){
                connecion.close();

                var vacancies = _.each(result, function(res){
                    return new Vacancy(res)
                });
                callback(null, vacancies);
            });

        }));
    }));
}
function getVacancyById(id, callback){
    db.getConnection(errTo(callback, function(connecion){
        r.table('vacancies').filter({id:id}).run(connecion, errTo(callback, function(cursor){

            cursor.toArray(function(err, result){
                connecion.close();

                var vacancies = _.each(result, function(res){
                    return new Vacancy(res)
                });
                callback(null, vacancies);
            });

        }));
    }));
}

module.exports = {
    baseClass:Vacancy,
    getVacancies:getVacancies,
    getVacancyById:getVacancyById
};
