var db = require('../schema/connection');
var errTo = require ('errto');
var r = require('rethinkdb');
var _ = require('underscore');

function Company(options){
    this.id = options.id;
    this.name = options.name;
    this.address = options.address;
    this.membersCount = options.memberCount;
}

function getCompanies(callback){
    db.getConnection(errTo(callback, function(connecion){

        r.table('companies').run(connecion, errTo(callback, function(cursor){
            cursor.toArray(function(error, result){
                connecion.close();

                var companies = _.each(result, function(res){
                    return new Company(res)
                });

                callback(null, companies);
            });

        }));
    }));
}

function getCompanyById(id, callback){
    db.getConnection(errTo(callback, function(connecion){

        r.table('companies').filter({id:id}).run(connecion, errTo(callback, function(cursor){

            cursor.toArray(function(error, result){
                connecion.close();

                var companies = _.each(result, function(res){
                    return new Company(res)
                });

                callback(null, companies);
            });

        }));
    }));
}

function getVacanciesById(id, callback){
    db.getConnection(errTo(callback, function(connecion){
        r.table('vacancies').filter({companyId:id}).run(connecion, errTo(callback, function(cursor){

            cursor.toArray(function(err, result){
                connecion.close();

                var companies = _.each(result, function(res){
                    return new Company(res)
                });

                callback(null, companies);
            });

        }));
    }));
}

module.exports = {
    baseClass:Company,
    getCompanies:getCompanies,
    getCompanyById:getCompanyById,
    getVacanciesById:getVacanciesById
};
