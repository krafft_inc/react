/* eslint-env es6 */
var React = require('react');
var Relay = require('react-relay');

class App extends React.Component {
  render() {
    return (
        <div>
          <h2>Company: {this.props.company1.name}</h2>
          <h2>Vacancies:</h2>
          <ul>
            {this.props.company1.vacancies.edges.map(edge =>
                    <li key={edge.node.id}>description: {edge.node.description} position: {edge.node.position})</li>
            )}
          </ul>

            <h2>Company: {this.props.company2.name}</h2>
            <h2>Vacancies:</h2>
            <ul>
              {this.props.company2.vacancies.edges.map(edge =>
                      <li key={edge.node.id}>description: {edge.node.description} position:  {edge.node.position})</li>
              )}
            </ul>
          <h2>Company: {this.props.company3.name}</h2>
          <h2>Vacancies:</h2>
          <ul>
            {this.props.company3.vacancies.edges.map(edge =>
                    <li key={edge.node.id}>description: {edge.node.description} position: {edge.node.position}</li>
            )}
          </ul>
        </div>
    )
  }
}

exports.Container1 = Relay.createContainer(App, {
  fragments: {
    company1: () => Relay.QL`
      fragment on Company {
        name,
        vacancies(first: 10) {
          edges {
            node {
              id,
              description,
              position,
            },
          },
        },
      }
    `,
    company2: () => Relay.QL`
      fragment on Company {
        name,
        vacancies(first: 10) {
          edges {
            node {
               id,
              description,
              position,
            },
          },
        },
      }
    `,
    company3: () => Relay.QL`
      fragment on Company {
        name,
        vacancies(first: 10) {
          edges {
            node {
                id,
              description,
              position,
            },
          },
        },
      }
    `,
  },
});

exports.queries = {
  name: 'AppQueries',
  params: {},
  queries: {
    company1: () => Relay.QL`query { company1 }`,
    company2: () => Relay.QL`query { company2 }`,
    company3: () => Relay.QL`query { company3 }`,
  },
}

